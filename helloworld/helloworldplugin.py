from openlp.core.common import translate
from openlp.core.lib import Plugin, StringContent

__default_settings__ = {}

class HelloWorldPlugin(Plugin):
    def __init__(self):
        super(HelloWorldPlugin, self).__init__('helloworld', __default_settings__)

    @staticmethod
    def about():
        """
        Plugin Hello World about method
        
        :return: text
        """
        about_text = translate('HelloWorldPlugin', '<strong>Hello World Plugin</strong>'
                               '<br />Hello, World!')
        return about_text 

    def set_plugin_text_strings(self):
        """
        Called to define all translatable texts of the plugin
        """
        # Name PluginList
        self.text_strings[StringContent.Name] = {
            'singular': translate('HelloWorldPlugin', 'Hello World', 'name singular')
        }
        # Name for MediaDockManager, SettingsManager
        self.text_strings[StringContent.VisibleName] = {
            'title': translate('HelloWorldPlugin', 'Hello World', 'container title')
        }
